// -*- mode: js2; indent-tabs-mode: nil; js2-basic-offset: 4 -*-

//    Focused window indicator
//    Puts icon and title of the current window in the gnome shell top panel
//
//    ----------------------------------------------------------------------
//    Copyright © 2018  Pellegrino Prevete
//
//    All rights reserved
//    ----------------------------------------------------------------------
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Clutter = imports.gi.Clutter;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Slider = imports.ui.slider.Slider;


// Translations
const Gettext = imports.gettext.domain('gnome-shell-extensions');
const _ = Gettext.gettext;

let focusWindowNotifyConnection = null;
let windowTitleNotifyConnection = null;
let maximizedNotifyConnection = null;
let SWI;
var FWI; 
let window;

class TransparencySlider extends PopupMenu.PopupBaseMenuItem {
    constructor(win) {
        super();
        this.compositor = win.get_compositor_private();

        // Icon
        this.icon = new St.Icon({ icon_name: 'keyboard-brightness' + '-symbolic',
                                  style_class: 'system-status-icon',
                                  x_align: Clutter.ActorAlign.START });
        this.icon.set_icon_size(16);
        this.actor.add_child(this.icon);

        // Slider
        this.slider = new Slider(0);
        this.slider.setMaximumValue(1);
        this.slider.setValue(this.compositor.opacity);
        this.slider.actor.visible = true;
        this.slider.connect('value-changed', this._sliderChanged.bind(this));
        this.slider.actor.accessible_name = _("Transparency");
        this.actor.add(this.slider.actor, { expand:true });
        this.actor.connect('button-press-event', (actor, event) => {
            return this.slider.startDragging(event);
        });
        this.actor.connect('key-press-event', (actor, event) => {
            return this.slider.onKeyPressEvent(actor, event);
        });
    }

    _sliderChanged(slider, value) {
        let percent = parseInt(value * 255);
        this.compositor.set_opacity(percent);
    }

}

class OpenWindow extends PopupMenu.PopupBaseMenuItem {
    constructor(win) {
        super();
        this.win = win;
        this.label = new St.Label({text: win.get_title(), x_expand: true});
        this.actor.add_child(this.label);
    }

    destroy() {
        super.destroy();
    }

    activate(event) {
        super.activate(event);

        // Switch to workspace of the input window
        let index = this.win.get_workspace().index();
        let workspaceManager = global.workspace_manager;
        let metaWorkspace = workspaceManager.get_workspace_by_index(index);
        metaWorkspace.activate(global.get_current_time());

        // Focus the input window
        this.win.activate(global.get_current_time());
    }
};

class Action extends PopupMenu.PopupBaseMenuItem {
    constructor(menu, appInfo, action) {
        super();
        this.appInfo = appInfo;
        this.action = action;
        this.menu = menu;
        this.label = new St.Label({ text: appInfo.get_action_name(action), x_expand: true });
        this.actor.add_child(this.label);
    }

    destroy() {
        super.destroy();
    }

    activate(event) {
        super.activate(event);
        this.appInfo.launch_action(this.action, null);
        this.menu.setOpenWindows.bind(this.menu)();
    }
};

class Control extends PopupMenu.PopupBaseMenuItem {
    constructor(control) {
        super();
        this.control = control;
        this.label = new St.Label({ text: control['text'], x_expand: true });
        this.actor.add_child(this.label);
        if ('ornament' in control) {
            this.setOrnament(control['ornament']);
        }
        else {
            this.setOrnament(PopupMenu.Ornament.NONE);
        }
    }

    destroy() {
        super.destroy();
    }

    activate(event) {
        super.activate(event);
        this.control['function']();
    }
};

class FocusWindowIndicator extends PanelMenu.Button {
    constructor() {
        super(0.0, _("Focused window indicator"));
        this.app = null;
        this.windows = null;
        this.openWindowsItems = {};
        this.controlsItems = {};
        this.controlsVisible = false;

        // Panel button
        // HBox
        this.hbox = new St.BoxLayout({ style_class: 'panel-status-menu-box',
                                       x_expand:true,
                                       x_align: Clutter.ActorAlign.START });
        this.actor.add_actor(this.hbox);
        this.hbox.set({'visible':false});

        // Icon
        this.icon = new St.Icon({ icon_name: 'applications-system' + '-symbolic',
                                  style_class: 'system-status-icon',
                                  x_align: Clutter.ActorAlign.START });
        this.hbox.add_child(this.icon);

        // Label
        this.label = new St.Label({ text: "Focused window indicator",
                                    y_expand: true,
                                    y_align: Clutter.ActorAlign.CENTER,
                                    x_expand: true,
                                    x_align: Clutter.ActorAlign.START });
        this.hbox.add_child(this.label);

        // Arrow
        this.hbox.add_child(PopupMenu.arrowIcon(St.Side.BOTTOM));

        // Popup menu
        // Open windows
        this.openWindows = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this.openWindows);

        // Separator
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // Application actions
        this.actions = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this.actions);

        // Separator
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // Window controls
        this.controls = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this.controls);

        // Separator
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // Close
        this.close = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this.close);

        // Connections
        focusWindowNotifyConnection = global.display.connect('notify::focus-window', this.onFocusWindowNotify.bind(this));
    }

    setOpenWindows() {
        this.openWindows.removeAll();
        this.openWindowsItems = {};
        let windows = this.windows.slice(0);
        if (windows.length > 1) {
            windows.sort(function(a,b){return a.title.localeCompare(b.title);});
        }
        windows.forEach((w) => {
            let openWindow = new OpenWindow(w);
            let menu = this.openWindows;
            this.openWindowsItems[w] = openWindow;
            menu.addMenuItem(openWindow);
        });
    }

    setLabel() {
        this.label.text = window.title;    
    }

    setControls() {
        this.controlsVisible = true;
        this.controls.removeAll();

        // Minimize
        let minimize;
        if (!window.minimized) {
            minimize = {'text':_("Minimize"),
                        'function':() => {window.minimize();
                                          this.setControls();}};
        }
        this.controls.addMenuItem(new Control(minimize));

        // Maximize
        let maximize;
        if (window.maximized_horizontally && window.maximized_vertically) {
            maximize = {'text':_("Unmaximize"),
                        'function':() => {window.unmaximize(Meta.MaximizeFlags.HORIZONTAL |
                                                            Meta.MaximizeFlags.VERTICAL);
                                          this.setControls();}};
        }
        else {
            maximize = {'text':_("Maximize"),
                        'function':() => {window.maximize(Meta.MaximizeFlags.HORIZONTAL |
                                                          Meta.MaximizeFlags.VERTICAL);
                                          this.setControls();}};
        }
        this.controls.addMenuItem(new Control(maximize));

        // Always on top
        let above;
        let transparency;
        if (!window.above) {
            above = {'text':_("Always on top"),
                     'function':() => {window.make_above();
                                       this.setControls();}};
            transparency = false;
        }
        else {
            above = {'text':_("Always on top"),
                     'ornament':PopupMenu.Ornament.CHECK,
                     'function':() => {window.unmake_above();
                                       this.setControls();}};
            transparency = true;
        }
        this.controls.addMenuItem(new Control(above));
        if (transparency) {
            this.controls.addMenuItem(new TransparencySlider(window));
        }

        // Close
        this.close.removeAll();
        let close = {'text':_("Close"),
                     'function':() => {window.delete(global.get_current_time());
                                       this.updateFocus();
                                       this.setOpenWindows.bind(this)();
                                      }
                    };
        this.close.addMenuItem(new Control(close));

        // Close all
        log(this.windows.length);
        if (this.windows.length > 1) {
            let exit = {'text':_("Close all"),
                        'function':() => {this.windows.forEach(w => {w.delete(global.get_current_time());});
                                          this.updateFocus();
                                          this.setOpenWindows.bind(this)();}
                       };
            this.close.addMenuItem(new Control(exit));
        }
    }

    updateFocus() {
        let result = [false, false];
        let app = Shell.WindowTracker.get_default().get_window_app(window);
        let windows = app.get_windows();
        if (app != this.app) {
            this.app = app;
            this.windows = windows;
            return [true, true];
        }
        if (windows != this.windows) {
                this.windows = windows;
                return [false, true];
        }
        return [false, false];
    }

    setWindowIndicator() {
        let updateApp, updateWindow;
        [updateApp, updateWindow] = this.updateFocus.bind(this)();
        if (updateApp) {
            let appInfo = this.app.app_info;

            // Set icon
            let iconName = appInfo.get_icon().names[0];
            this.icon.icon_name = iconName + "-symbolic";

            // Set actions
            let actions = appInfo.list_actions();
            this.actions.removeAll();
            actions.forEach(a => {
                this.actions.addMenuItem(new Action(this, appInfo, a));
            });
        }
        if (updateWindow) {

            // Set open windows
            this.setOpenWindows.bind(this)();

            // Set current window
            this.windows.forEach(w => {
                let item = this.openWindowsItems[w];
                if (w == window) {
                    item.setOrnament(PopupMenu.Ornament.DOT);
                }
                else {
                    item.setOrnament(PopupMenu.Ornament.NONE);
                }
            });

            // Set controls
            this.setControls();
        }

        // Set label
        this.setLabel.bind(this)();
    }
   
    onFocusWindowNotify() {

        // Disconnect previous title notify signals
        if((windowTitleNotifyConnection)&&(window))
            window.disconnect(windowTitleNotifyConnection);

        // Get current window
        window = global.display.get_focus_window();

        // If no focused window hide the menu
        if(!window) {
            this.hbox.set({'visible':false});
            return;
        }

        // Connect window signals to indicator
        SWI = this.setWindowIndicator.bind(this);
        windowTitleNotifyConnection = window.connect("notify::title", this.setLabel.bind(this));
        maximizedNotifyConnection = window.connect('notify::maximized-horizontally', SWI);
       
        // Set window indicator
        SWI();

        // Set the menu visible
        this.hbox.set({'visible':true});
        if (!this.controlsVisible) {
            this.setControls();
        }

    }

    destroy() {
        global.display.disconnect(focusWindowNotifyConnection);
        window.disconnect(maximizedNotifyConnection);
        window.disconnect(windowTitleNotifyConnection);
        super.destroy();
    }

};


function init(metadata) {
    log('Extension initialized');
}

function enable() {
    FWI = new FocusWindowIndicator();
    Main.panel.addToStatusArea('focused-window-indicator', FWI, 1, 'left');
    FWI.onFocusWindowNotify();
}

function disable() {
    FWI.destroy()
}
