# Focused window indicator

[![License: GPL v3+](https://img.shields.io/badge/license-GPL%20v3%2B-blue.svg)](http://www.gnu.org/licenses/gpl-3.0) 
[![Javascript Support](https://img.shields.io/badge/gjs-1.50.x-orange.svg)](https://gitlab.gnome.org/GNOME/gjs/wikis/Home)

![Focused window indicator](screenshot.png)

*Focused window indicator* is a GNOME shell extension that puts a focused window 'icon + title' labeled menu in the top bar, containing WM controls and both a list of opened window and available actions from the same application. Parts of the features of this extension were provided by the 3.32-deprecated [application menu](https://wiki.gnome.org/Design/OS/AppMenu).

The features of this extensions are taken by suggestion from the GNOME Shell's 'application menu removal' [issue](https://gitlab.gnome.org/GNOME/gnome-shell/issues/624).

## Installation

You can install it through [GNOME Extensions](https://extensions.gnome.org/extension/1496/focused-window-indicator/) webpage.
If you want to install the version on this repo just type these commands in your terminal:

    git clone https://gitlab.com/tallero/focused-window-indicator
    cp -r focused-window-indicator/focused-window-indicator\@prevete.ml ~/.local/share/gnome-shell/extensions
    gnome-shell-extension-tool -e focused-window-indicator@prevete.ml

If still not working, restart `gnome-shell` pressing `Alt+F2` and then executing `r`.

## About

This program is licensed under [GNU General Public License v3 or later](https://www.gnu.org/licenses/gpl-3.0.en.html) by [Pellegrino Prevete](http://prevete.ml). If you find this program useful, consider offering me a [beer](https://patreon.com/tallero), a new [computer](https://patreon.com/tallero) or a part time remote [job](mailto:pellegrinoprevete@gmail.com) to help me pay the bills.

